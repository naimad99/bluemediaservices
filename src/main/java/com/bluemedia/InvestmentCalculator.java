package com.bluemedia;

import com.bluemedia.domain.Pair;
import com.bluemedia.domain.FundsTypes;
import com.bluemedia.domain.Fund;
import com.google.common.collect.Ordering;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;
import java.util.stream.Stream;


/**
 *
 * @author dskobelx
 */
public class InvestmentCalculator {
    
    public Pair<List<Fund>, Integer> compute(int budget, Map<FundsTypes, Double> investmentStrategy, List<Fund> input) {
        List<Fund> calculatedFunds = calculateFunds(budget, investmentStrategy, input);
        int unusedAmount = calculateUnusedAmount(budget, calculatedFunds);
        return Pair.of(calculatedFunds, unusedAmount);
    }
    
    interface Calculator<T, U, V, R> {
        R apply(T budget, U percent, V fundsSize);
    }
    
    final private Calculator<Integer, Double, Integer, Function<Fund, Fund>> calculate = (budget, percent, fundsSize) -> {
        double amountOfMoneyForGroupedFunds = budget * percent;
        double amountOfMoneyForFund = amountOfMoneyForGroupedFunds / fundsSize;
        int bonusAmountOfMoney = (int) (amountOfMoneyForGroupedFunds - ((int) amountOfMoneyForFund * fundsSize));
        AtomicInteger bonus = new AtomicInteger(bonusAmountOfMoney);
        return (Function<Fund, Fund>) (it) -> {
            double amount = amountOfMoneyForFund + bonus.getAndSet(0);
            it.setAmount((int) amount);
            it.setPercentage(new DecimalFormat("0.##").format(it.getAmount() * 100f / budget) + "%");
            return it;
        };
    };
    
    private List<Fund> calculateFunds(int budget, Map<FundsTypes, Double> investmentStrategy, List<Fund> input) {
        Map<FundsTypes, List<Fund>> fundsGroupedByTypes = input.stream().collect(Collectors.groupingBy(Fund::getType));
        List<Fund> calculatedFunds = fundsGroupedByTypes.entrySet().stream().
                flatMap(fund -> calculateAmountsForFunds(budget, investmentStrategy, fund.getKey(), fund.getValue())).collect(toList());
        Ordering<Fund> expectedOrder = Ordering.explicit(input);
        List<Fund> sortedOutputFunds = expectedOrder.sortedCopy(calculatedFunds);
        return sortedOutputFunds;
    }
    
    private int calculateUnusedAmount(int budget, List<Fund> calculatedFunds) {
        int totalAmountUsed = calculatedFunds.stream().map(it -> it.getAmount()).reduce(0, (agg, item) -> agg + item);
        return budget - totalAmountUsed;
    } 
    
    private Stream<Fund> calculateAmountsForFunds(int budget, Map<FundsTypes, Double> strategies, FundsTypes fundType, List<Fund> funds) {
        Function<Fund, Fund> partial = calculate.apply(budget, strategies.get(fundType), funds.size());
        return funds.stream().map(i -> partial.apply(i));
    }

}
