package com.bluemedia.domain;

import com.google.common.collect.ImmutableMap;
import java.util.Map;

/**
 *
 * @author dskobelx
 */
public class Strategies {
    public static Map<FundsTypes, Double> SAFE = ImmutableMap.of(
            FundsTypes.POLISH, 0.2, 
            FundsTypes.FOREIGN, 0.75, 
            FundsTypes.MONETARY, 0.05);
    public static Map<FundsTypes, Double> BALANCED = ImmutableMap.of(
            FundsTypes.POLISH, 0.3, 
            FundsTypes.FOREIGN, 0.6, 
            FundsTypes.MONETARY, 0.1);
    public static Map<FundsTypes, Double> AGGRESSIVE = ImmutableMap.of(
            FundsTypes.POLISH, 0.4, 
            FundsTypes.FOREIGN, 0.2, 
            FundsTypes.MONETARY, 0.4);
}
