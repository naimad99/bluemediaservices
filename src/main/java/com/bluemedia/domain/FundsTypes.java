package com.bluemedia.domain;

/**
 *
 * @author dskobelx
 */
public enum FundsTypes {
    POLISH, FOREIGN, MONETARY
}
