package com.bluemedia.domain;

import java.util.Objects;


/**
 *
 * @author dskobelx
 */
public class Fund {
    private long id;
    private String name;
    private FundsTypes type;
    private int amount;
    private String percentage;
    
    public Fund(){}
    
    public Fund(long id, FundsTypes type, String name) {
        this.id = id;
        this.type = type;
        this.name = name;
    }
    
    public Fund(long id, FundsTypes type, String name, int amount, String percentage) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.amount = amount;
        this.percentage = percentage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FundsTypes getType() {
        return type;
    }

    public void setType(FundsTypes type) {
        this.type = type;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 79 * hash + Objects.hashCode(this.name);
        hash = 79 * hash + Objects.hashCode(this.type);
        hash = 79 * hash + this.amount;
        hash = 79 * hash + Objects.hashCode(this.percentage);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Fund other = (Fund) obj;
        if (this.id != other.id) {
            return false;
        }
        if (this.amount != other.amount) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.percentage, other.percentage)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Fund{" + "id=" + id + ", name=" + name + ", type=" + type + ", amount=" + amount + ", percentage=" + percentage + '}';
    }
    
}
