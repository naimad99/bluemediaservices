package com.bluemedia.domain;

/**
 *
 * @author dskobelx
 */
public class Pair<U, T> {
    final private U first;
    final private T second;
    
    public Pair(U first, T second) {
        this.first = first;
        this.second = second;
    }
    
    public static <U, T> Pair<U, T> of(U first, T second) {
        return new Pair<>(first, second);
    }

    public U getFirst() {
        return first;
    }

    public T getSecond() {
        return second;
    }
    
}
