package com.bluemedia;

import com.bluemedia.domain.Fund;
import com.bluemedia.domain.Strategies;
import com.bluemedia.domain.Pair;
import com.bluemedia.domain.FundsTypes;
import static java.util.Arrays.asList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;


/**
 *
 * @author dskobelx
 */
public class InvestmentCalculatorTests {
    final private InvestmentCalculator investmentCalculator = new InvestmentCalculator();
    
    @Test
    public void testNoUnused() {
        List<Fund> input = asList(new Fund(1, FundsTypes.POLISH, "fundusz polski 1"),
                new Fund(2, FundsTypes.POLISH, "fundusz polski 2"),
                new Fund(3, FundsTypes.FOREIGN, "fundusz zagraniczny 1"),
                new Fund(4, FundsTypes.FOREIGN, "fundusz zagraniczny 2"),
                new Fund(5, FundsTypes.FOREIGN, "fundusz zagraniczny 3"),
                new Fund(6, FundsTypes.MONETARY, "fundusz pieniezny 1")
        );
        List<Fund> expectedList = asList(new Fund(1, FundsTypes.POLISH, "fundusz polski 1", 1000, "10%"),
                new Fund(2, FundsTypes.POLISH, "fundusz polski 2", 1000, "10%"),
                new Fund(3, FundsTypes.FOREIGN, "fundusz zagraniczny 1", 2500, "25%"),
                new Fund(4, FundsTypes.FOREIGN, "fundusz zagraniczny 2", 2500, "25%"),
                new Fund(5, FundsTypes.FOREIGN, "fundusz zagraniczny 3", 2500, "25%"),
                new Fund(6, FundsTypes.MONETARY, "fundusz pieniezny 1", 500, "5%")
        );
        
        Pair<List<Fund>, Integer> actualResult = investmentCalculator.compute(10000, Strategies.SAFE, input);

        Assert.assertEquals(expectedList, actualResult.getFirst());
        Assert.assertEquals(0, actualResult.getSecond().intValue());
    }
    
    @Test
    public void testOneUnused() {
        List<Fund> input = asList(
                new Fund(1, FundsTypes.POLISH, "fundusz polski 1"),
                new Fund(2, FundsTypes.POLISH, "fundusz polski 2"),
                new Fund(3, FundsTypes.FOREIGN, "fundusz zagraniczny 1"),
                new Fund(4, FundsTypes.FOREIGN, "fundusz zagraniczny 2"),
                new Fund(5, FundsTypes.FOREIGN, "fundusz zagraniczny 3"),
                new Fund(6, FundsTypes.MONETARY, "fundusz pieniezny 1")
        );
        List<Fund> expectedList = asList(
                new Fund(1, FundsTypes.POLISH, "fundusz polski 1", 1000, "10%"),
                new Fund(2, FundsTypes.POLISH, "fundusz polski 2", 1000, "10%"),
                new Fund(3, FundsTypes.FOREIGN, "fundusz zagraniczny 1", 2500, "25%"),
                new Fund(4, FundsTypes.FOREIGN, "fundusz zagraniczny 2", 2500, "25%"),
                new Fund(5, FundsTypes.FOREIGN, "fundusz zagraniczny 3", 2500, "25%"),
                new Fund(6, FundsTypes.MONETARY, "fundusz pieniezny 1", 500, "5%")
        );
        
        Pair<List<Fund>, Integer> actualResult = investmentCalculator.compute(10001, Strategies.SAFE, input);

        Assert.assertEquals(expectedList, actualResult.getFirst());
        Assert.assertEquals(1, actualResult.getSecond().intValue());
    }
    
    @Test
    public void testUnevenGroupInvestment() {
        List<Fund> input = asList(new Fund(1, FundsTypes.POLISH, "fundusz polski 1"),
                new Fund(2, FundsTypes.POLISH, "fundusz polski 2"),
                new Fund(3, FundsTypes.POLISH, "fundusz polski 3"),
                new Fund(4, FundsTypes.FOREIGN, "fundusz zagraniczny 1"),
                new Fund(5, FundsTypes.FOREIGN, "fundusz zagraniczny 2"),
                new Fund(6, FundsTypes.MONETARY, "fundusz pieniezny 1")
        );
        List<Fund> expectedList = asList(new Fund(1, FundsTypes.POLISH, "fundusz polski 1", 668, "6.68%"),
                new Fund(2, FundsTypes.POLISH, "fundusz polski 2", 666, "6.66%"),
                new Fund(3, FundsTypes.POLISH, "fundusz polski 3", 666, "6.66%"),
                new Fund(4, FundsTypes.FOREIGN, "fundusz zagraniczny 1", 3750, "37.5%"),
                new Fund(5, FundsTypes.FOREIGN, "fundusz zagraniczny 2", 3750, "37.5%"),
                new Fund(6, FundsTypes.MONETARY, "fundusz pieniezny 1", 500, "5%")
        );
        
        Pair<List<Fund>, Integer> actualResult = investmentCalculator.compute(10000, Strategies.SAFE, input);

        Assert.assertEquals(expectedList, actualResult.getFirst());
        Assert.assertEquals(0, actualResult.getSecond().intValue());
    }
    
    @Test
    public void testMissingFundType() {
        List<Fund> input = asList(new Fund(1, FundsTypes.POLISH, "fundusz polski 1"),
                new Fund(2, FundsTypes.POLISH, "fundusz polski 2"),
                new Fund(3, FundsTypes.POLISH, "fundusz polski 3"),
                new Fund(4, FundsTypes.FOREIGN, "fundusz zagraniczny 1"),
                new Fund(5, FundsTypes.FOREIGN, "fundusz zagraniczny 2")
        );
        List<Fund> expectedList = asList(new Fund(1, FundsTypes.POLISH, "fundusz polski 1", 668, "6.68%"),
                new Fund(2, FundsTypes.POLISH, "fundusz polski 2", 666, "6.66%"),
                new Fund(3, FundsTypes.POLISH, "fundusz polski 3", 666, "6.66%"),
                new Fund(4, FundsTypes.FOREIGN, "fundusz zagraniczny 1", 3750, "37.5%"),
                new Fund(5, FundsTypes.FOREIGN, "fundusz zagraniczny 2", 3750, "37.5%")
        );
        
        Pair<List<Fund>, Integer> actualResult = investmentCalculator.compute(10000, Strategies.SAFE, input);

        Assert.assertEquals(expectedList, actualResult.getFirst());
        Assert.assertEquals(500, actualResult.getSecond().intValue());
    }
    
    @Test
    public void testMissingFunds() {
        List<Fund> input = asList(
        );
        List<Fund> expectedList = asList(
        );
        
        Pair<List<Fund>, Integer> actualResult = investmentCalculator.compute(10000, Strategies.SAFE, input);

        Assert.assertEquals(expectedList, actualResult.getFirst());
        Assert.assertEquals(10000, actualResult.getSecond().intValue());
    }
}
