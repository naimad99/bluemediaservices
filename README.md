Projekt ma na celu dostarczyć próbkę kodu aby można było ocenić czy warto przechodzić do kolejnego etapu.  
W związku z tym projekt zawiera kilka uproszczeń.  
1. Zamiast BigDecimal użyto mało precyzyjne typy liczbowe  
2. Nie pokryto testami wszystkich metod  
3. Starano się nie dokładać na siłę warstw abstrakcji  

